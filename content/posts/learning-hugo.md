---
title: "Learning Hugo"
date: 2018-03-04T18:12:37-08:00
draft: false
---
I've decided to take on learning Hugo to better establish my web presence.

I'm hoping to get some stuff up here and on GitLab. Most of my content will likely involve Ansible, Docker, and other DevOps stuff.

My first few posts will all be around learning this new static site generator though.

I'll be checking in the code to <a href="https://gitlab.com/magentashades"> Gitlab on the regular.</a>


